// Copyright 2018 Saki Matsuura
// This software may be modified and distributed under the terms
// of the MIT license

// 音声を再生するためのブラウザのオブジェクト
window.AudioContext = window.AudioContext || window.webkitAudioContext;
var context = new AudioContext();

// 音声合成のためのURL + 認証のためのAPI KEY
var g_url = "https://texttospeech.googleapis.com/v1beta1/text:synthesize?key=AIzaSyA4cCa3vYf8P4C1IMvrr31Uc1ml6YrpyGI"

// HTTPリクエストのためのヘッダ
var hd = {
    'Content-Type': 'application/json; charset=utf-8',
}

// サウンドを再生
var playSound = function (buffer) {
    // source を作成
    var source = context.createBufferSource();
    // buffer をセット
    source.buffer = buffer;
    // context に connect
    source.connect(context.destination);
    // 再生（非同期的）
    source.start(0);
};

// main: Webページがブラウザに読み込まれたときに呼び出される
window.onload = function () {

    // base64形式のデータをArrayBufferに変換する関数
    function _base64ToArrayBuffer(base64) {
        var binary_string = window.atob(base64);
        var len = binary_string.length;
        var bytes = new Uint8Array(len);
        for (var i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

    // textboxの文字列が変化したとき（Enterを押したとき）
    // このtbは、html06のなかにある<input id="tb" ... >を指している
    tb.onchange = function () {

        // WebSocket を作成
        const socket = new WebSocket(tb.value);
        console.log('create a new websocket')

        socket.addEventListener('message', function (event) {

            

            // Google APIに送るデータ
            var msg = JSON.stringify({
                'input': { 'text': event.data },
                'voice': { 'languageCode': 'ja-JP' },
                'audioConfig': { 'audioEncoding': 'MP3' }
            })

            // HTTPリクエストを送るためのオブジェクトを生成する
            var req = new XMLHttpRequest()

            // HTTPリクエストの応答のデータ型を設定する
            req.responseType = 'json';

            // HTTPリクエストのためのコールバック関数を設定する
            // HTTPリクエストの応答(req.response)が返ってきたら(req.readyState === 4)、音声を再生する(playSound)
            // 1. req.responseにはJSON形式のデータが入っている
            // 2. req.response.audioContentには、音声データを表すデータがbase64形式で入っている。
            //    base64形式に変換しているのは、HTTPではバイナリデータを送受信できないため、バイナリデータをテキスト（文字）に
            //    変換する必要があるからである。
            // 3. _base64ToArrayBuffer(req.response.audioContent)で、req.response.audioContentのデータをjavascriptの
            //    arraybuffer形式に変換する。これは、context.decodeAudioDataに渡すデータ（第1引数）はarraybufferでなければならないためである
            // 4. context.decodeAudioData()は、第1引数で渡されたデータの解析に成功したら、第2引数で渡された関数に音声データを渡して呼び出す
            // 5. playSound()で音声を再生する。音声の再生は非同期的なので、音声の再生が終わる前に関数から返る。
            req.onreadystatechange = function () {
                // HTTPレスポンスを受け取ったら
                if (req.readyState === 4) {
                    // レスポンスのステータスが正常ならば
                    if (req.status === 0 || req.status === 200) {
                        context.decodeAudioData(_base64ToArrayBuffer(req.response.audioContent), function (buffer) {
                            // 音声を再生する
                            playSound(buffer)
                        });
                    }
                }
            };

            // HTTPリクエストのメソッドとURLを設定する、POST:データの送信
            req.open('POST', g_url, true);

            // HTTPリクエストのヘッダを設定する
            req.setRequestHeader('Content-Type', 'application/json; charset=utf-8')
            //req.setRequestHeader('Referer', 'http://www.sougoupbl.kyutech.ac.jp/') 標準ではセキュリティのためRefererを設定できない

            // HTTPリクエストを送る
            // 送った後この関数を抜けてしまうが、コールバック関数を設定しているのでHTTPリクエストの応答が返ってきたときに
            // req.onreadystatechangeに設定した関数が呼び出される。
            req.send(msg);

            console.log(msg)
        });
        socket.addEventListener('error', function (event) {
            console.log('websocketにエラーが発生しました')
        });

    }
};