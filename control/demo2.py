import time
import random
import threading
from websocket import create_connection

'''
Script: ???
'''

'''
print('コネクト')
ws = create_connection("ws://192.168.0.8:9999/")
'''


class Matter(object):
    '''
    class Matter:  何とかクラス
    '''
    def oha(self):
        '''
        何とか関数
        '''
        print('ohayou')
        self.reset_tr()
        print (self.state)

    def oya(self):
        print('oyasumi')
        self.reset_tr()
        print (self.state)

    def gannbatte(self):
        print('gannbatte')

    def ittera(self):
        ##ws.send('itterassyai')
        ##self.reset_tr()
        ##print (self.state)
        x = random.randrange(2)

        if x == 0:
            print('itterassyai')
            self.reset_tr()
            ##lump.tr_ittera()

        if x == 1: 
            print('kiwotukete')
            ##lump.tr_kiwotuketene()
        
        if x == 2:
            print('gannbatte')
            ##lump.tr_gannbatte()
        
    def okaeri(self):
        print('okaeri')
        self.time_tr()
        print (self.state)      #(lump.state)→(self.state)

    def otukare(self):
        print('otukare')
        self.time_tr()
        print (self.state) 

    def hima(self):
        x = random.randrange(19)

        if x == 0:
            print('nene')
            self.hennji_tr()

        if x == 1:
            print('nann')
            self.hennji_tr()

        t = threading.Timer(5,self.hima)
        t.start()

    def hennji(self):
        x = random.randrange(1)

        if x == 0:
             print('soonannya')
             self.time_tr()
        
        if x == 1:
             print('gannbaroune')
             self.time_tr()

    


lump = Matter()

from transitions import Machine

states = ['wait','sensor','voice','time']

transitions = [

    { 'trigger': 'tr_oha', 'source': 'wait', 'dest': 'voice' , 'after': 'oha' },
    { 'trigger': 'tr_oya', 'source': 'time', 'dest': 'wait' , 'after': 'oya' },
    { 'trigger': 'tr_gannbatte', 'source': 'wait', 'dest': 'voice' ,'after': 'gannbatte'},
    { 'trigger': 'tr_okaeri', 'source': 'wait', 'dest':'sensor','after': 'okaeri' },
    { 'trigger': 'tr_otukare', 'source': 'wait', 'dest':'sensor','after': 'otukare' },
    { 'trigger': 'tr_ittera', 'source': 'wait', 'dest':'sensor' , 'after': 'ittera' },
    { 'trigger': 'tr_0', 'source': 'wait', 'dest': 'sensor' ,'after': 'ittera'},


    { 'trigger': 'time_tr', 'source': 'sensor', 'dest': 'time' , 'after':'hima' },
    { 'trigger': 'time_tr', 'source': 'time', 'dest': 'time' , 'after':'hima' },
    { 'trigger': 'hennji_tr', 'source': 'time', 'dest': 'time' , 'after':'hennji' },
    { 'trigger': 'reset_tr', 'source': 'voice', 'dest': 'wait' },
    { 'trigger': 'reset_tr', 'source': 'sensor', 'dest': 'wait' }

]

machine = Machine(lump, states=states, transitions=transitions, initial='wait')

while True:
    msg = input()

    print("%s" % (msg))


    if msg == '音声:おはよう':
        lump.tr_oha()
    
    if msg == '音声:おやすみ':
        lump.tr_oya()
    
    if msg == '音声:学校休みたい':
        lump.tr_gannbatte()

    if msg == 'センサ:0':
        lump.tr_0()

    if msg == 'センサ:1':
        x = random.randrange(2)

        if x == 0:
            lump.tr_okaeri()

        elif x == 1: 
            lump.tr_otukare()

    else:
        lump.tr_sorena()